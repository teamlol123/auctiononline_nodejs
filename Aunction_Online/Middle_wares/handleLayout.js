var q = require('q');
var userModel = require('../Models/userModel');
var categoryModel = require('../Models/categoryModel');

module.exports = function(req, res, next) {

  if(req.session.idUser){
  var idUser = {
    idUser: req.session.idUser,
  }
    q.all([
      categoryModel.loadAll(),
    	userModel.getUserByIDUser(idUser),
	]).spread(function(cRows, rows) {
    rows[0].UserAsking = rows[0].UserAsking == 0? false: true
		res.locals.layoutVM = {
			user: rows[0],
      categories: cRows,
			// suppliers: []
		};
    	next();
    });
  }
  else{
    q.all([
      categoryModel.loadAll(),
	]).spread(function(rows) {
		res.locals.layoutVM = {
      categories: rows,
			// suppliers: []
		};
    	next();
    });
  }
}
