var express = require('express'),
    handlebars = require('express-handlebars'),
    bodyParser = require('body-parser'),
    handlebars_sections = require('express-handlebars-sections'),
    morgan = require('morgan'),
    path = require('path'),
    wnumb = require('wnumb'),
    session = require('express-session'),
    MySQLStore = require('express-mysql-session')(session),
    listProduct = require('./Controllers/listProductController'),
    homeController = require('./Controllers/homeController'),
    signupController = require('./Controllers/signupController'),
    signinController = require('./Controllers/signinController'),
    userinfoController = require('./Controllers/userinfoController'),
    detailProductController = require('./Controllers/detailProductController'),
    logoutController = require('./Controllers/logoutController'),
    searchController = require('./Controllers/searchController'),
    adminController = require('./Controllers/adminController'),
    handleLayout = require('./Middle_wares/handleLayout');


var app = express();
var options = {
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'baotuan',
    database: 'auction',
    createDatabaseTable: true,
    // expiration: 8640000,
    // checkExpirationInterval: 900000,
};
var sessionStore = new MySQLStore(options);

app.use(morgan('dev'));

app.engine('hbs', handlebars({
    extname: 'hbs',
    defaultLayout: 'main',
    layoutsDir: 'views/_layouts/',
    partialsDir: 'views/_partials/',
    helpers: {
      section: handlebars_sections(),
      number_format: function (n) {
          var nf = wnumb({
              thousand: ',',
              prefix: '$ ',
          });
          return nf.to(n);
      },
      enCode: function(str){
        temp = str.slice(3);
        temp = '***' + temp ;
        return temp;
      }
    }
}));
app.set('view engine', 'hbs');

app.use(express.static(
    path.resolve(__dirname, 'Public')
));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(session({
    key: 'session_cookie_name',
    secret: 'Z7X7gXzoKBT8h18jwXBEP4T0kJ8=',
    store: sessionStore,
    resave: false,
    saveUninitialized: true,
}));

app.use(handleLayout);
app.use("/", homeController);
app.use("/listProduct", listProduct);
app.use("/signUp", signupController);
app.use("/signIn", signinController);
app.use("/detail", detailProductController);
app.use("/logout", logoutController);
app.use('/search', searchController);
app.use("/userInfo", userinfoController);
app.use("/adminLogin", adminController);

app.listen(3000);
