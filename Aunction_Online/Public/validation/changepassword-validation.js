$(function() {
	$("#ChangePassword").validate({
		rules: {
			inputOldPass: {
				required: true,
				minlength: 6
			},
			inputNewPass: {
				required: true,
				minlength: 6
			},
			inputRePass: {
				required: true,
				equalTo: "#inputNewPass"
			}
		},
		messages: {
			inputOldPass: {
				required: "Please enter your old Password",
				minlength: "Your Password must be atleast 6 character long"
			},
			inputNewPass: {
				required: "Please enter your New Password",
				minlength: "Your Password must be atleast 6 character long"
			},
			inputRePass: {
				required: "Please re-enter your New Password",
			}
		}
	});
});