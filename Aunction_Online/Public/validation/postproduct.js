$(function() {
	$("#PostProduct").validate({
		rules: {
			title: {
				required: true,
			},
			catID: {
				required: true,
			},
			photos:{
				accept:"image/*",
			},
			description:{
				required: true,
			},
			bidPrice:{
				required: true,
			},
			bidJump:{
				required: true,
			},
			instantPrice:{
				required: true,
			},
			dayEnd:{
				required:true,
			}
		},
		messages: {
			title: {
				required: "Please enter a title"
			},
			catID: {
				required: "Please choose a category",
			},
			photos:{
				accept: "Please upload a image",
			},
			description:{
				required: "Please enter description of product",
			},
			bidPrice:{
				required: "Please enter bid price",
			},
			bidJump:{
				required: "Please enter bid jump",
			},
			instantPrice:{
				required: "Please enter buy price",
			},
			dayEnd:{
				required: "Please choose days listing",
			}
		},
		// submitHandler: function(form) {
		// 	form.submit();
		// }
	});
});
