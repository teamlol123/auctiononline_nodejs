$(function() {
	$("#UserInfo").validate({
		rules: {
			inputName: "required",
			inputEmail: {
				required : true,
				email: true
			},
			inputAddress: "required"
		},
		messages: {
			inputName: "Please enter your name",
			inputEmail: {
				required: "Please enter your E-mail",
				email: "Please enter a valid E-mail address"
			},
			inputAddress: "Please enter your Home Address"
		}
	});
});