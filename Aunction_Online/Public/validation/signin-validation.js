$(function() {
	$("#SignIn").validate({
		rules: {
			inputUsername: "required",
			inputPassword: {
				required : true,
			}
		},
		messages: {
			inputUsername: 'Please type in your Username',
			inputPassword: {
				required: "Please type in your Password",
			}
		}
	});
});