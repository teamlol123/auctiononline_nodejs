var express = require('express');
var multer = require('multer');
var path = require('path');
var datetime = require('node-datetime');
var categoryModel = require('../Models/categoryModel');
var productModel = require('../Models/productModel');

var route = express.Router();

var storage = multer.diskStorage({
	destination: function(req, file, callback) {
		callback(null, './Public/uploads')
	},
	filename: function(req, file, callback) {
		callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
	}
});

route.get("/", function (req, res) {
	if(res.locals.layoutVM.user === undefined){
		res.redirect('/signIn');
	}
	else if (res.locals.layoutVM.user.Permission == 1){
		categoryModel.loadAll().then(function (rows) {
			var vm ={
				category: rows,
				hasUser: res.locals.layoutVM.user === undefined,
				layoutVM: res.locals.layoutVM,
			};
			res.render("product/listProduct", vm);
		});
	}
});

route.post("/",  function (req, res) {
  var upload = multer({
  		storage: storage
  	}).array("photos",'3')
  	upload(req, res, function(err){
      //console.log(req.file.filename);
  		// res.send('<img src = "/uploads/'+req.file.filename+'"/>')

			//get image's filename
			switch(req.files.length){
				case 1:
				{
					req.body.mainImage = '/uploads/' + req.files[0].filename;
					req.body.photo1 = null;
					req.body.photo2 = null;
				}break;
				case 2:
				{
					req.body.mainImage = '/uploads/' + req.files[0].filename;
					req.body.photo1 = '/uploads/' + req.files[1].filename;
					req.body.photo2 = null;
				}break;
				case 3:
				{
					req.body.mainImage = '/uploads/' + req.files[0].filename;
					req.body.photo1 = '/uploads/' + req.files[1].filename;
					req.body.photo2 = '/uploads/' + req.files[2].filename;
				}break;
			}


			//get today & day end
			var dayStart = datetime.create();
			var dayEnd = datetime.create();
			dayEnd.offsetInDays(parseInt(req.body.dayEnd));
			var formatDayStart = dayStart.format('m-d-Y H:M:S');
			var formatDayEnd = dayEnd.format('m-d-Y H:M:S');

			//set TimeStart & TimeEnd
			req.body.TimeStart = formatDayStart;
			req.body.TimeEnd = formatDayEnd;

			req.body.idUser = req.session.idUser;
			productModel.insertProduct(req.body).then(function (data) {
				res.redirect("/detail/" + data);
			});
  	});
});
module.exports = route;
