var express = require('express');
var	userModel = require('../Models/userModel');
var SHA256 = require("crypto-js/sha256");
var CryptoJS = require("crypto-js");
var request = require('request');
var q = require('q');

var route = express.Router();


route.get('/', function(req, res) {
  var vm = {
    hasUser: res.locals.layoutVM.user === undefined,
    layoutVM: res.locals.layoutVM,
  };
    res.render('home/signUp',vm);
});

route.post('/', function(req, res) {
  if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
    return res.json({"responseCode" : 1,"responseDesc" : "Please select captcha"});
  }
  // Put your secret key here.
  var secretKey = "6LcCSiMUAAAAAA6XjIh9_gAj6V7jwzv9Jwa9UDC4";
  // req.connection.remoteAddress will provide IP address of connected user.
  var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
  // Hitting GET request to the URL, Google will respond with success or error scenario.
  request(verificationUrl,function(error,response,body) {
    body = JSON.parse(body);
    console.log(body);
    //Success will be true or false depending upon captcha validation.
    if(body.success !== undefined && !body.success) {
      // return res.json({"responseCode" : 1,"responseDesc" : "Failed captcha verification"});
    }
    q.all([userModel.getUserbyUsername(req.body), userModel.getUserbyEmail(req.body)])
    .spread(function (rows, rowsEmail) {
          if(rows.length === 0 && rowsEmail.length == 0) {
          userModel.getMaxIDuser().then(function (rows) {
            if (rows.length == 0){
                req.body.idUser = 1;
            }
            else{
              req.body.idUser = rows[0].idUser + 1;
            }
            req.body.inputPassword = SHA256(req.body.inputPassword);
            userModel.insertUser(req.body).then(function(data) {
              res.redirect('/signIn');
            });
        });
    }else{
      var vm = {
            hasUser: res.locals.layoutVM === undefined,
            layoutVM: res.locals.layoutVM,
            hasError : true,
            error: "Please choose a different Username or Email"
          };
         res.render('home/signUp',vm);
    }
    })
  });
});


module.exports = route;
