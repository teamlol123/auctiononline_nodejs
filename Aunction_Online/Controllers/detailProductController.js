var express = require('express');
var productModel = require('../Models/productModel');
var userModel = require('../Models/userModel');
var datetime = require('node-datetime');
var q = require('q');

var route = express.Router();

route.get('/:id', function (req, res) {
  q.all([productModel.getProductByID(req.params), productModel.getTotalWatching(req.params), productModel.bidLog(req.params), productModel.getTurnBid(req.params)])
  .spread(function (rows, total, bidLog, turn) {
    if(!rows[0].Photo1){
      rows[0].Photo1 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
      rows[0].Photo2 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
    }
    else if (!rows[0].Photo2) {
      rows[0].Photo2 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
    }
    var date = new Date(rows[0].TimeEnd);
    rows[0].TimeEnd = date;
    var vm = {
      total: total[0].Total,
      product: rows[0],
      bidLog: bidLog,
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
      currentBid: bidLog[bidLog.length - 1],
      isCurrentBid: bidLog.length == 0,
      turn: turn[0]
    };
    res.render('product/detailProduct', vm);
  })
})

//BIDDDDDDDDDDDDDDDDD
route.post('/:id',function (req, res) {
  q.all([productModel.getProductByID(req.params), productModel.getMaxBidPrice(req.params), userModel.getFeedbackPosByUser(req.session), userModel.getFeedbackNegByUser(req.session)])
  .spread(function (product, maxPrice, fbPos, fbNeg) {
    console.log(fbPos);
    console.log(fbNeg);
    if(fbPos.length == 0 && fbNeg.length == 0){
      if(maxPrice[0].idAuction !== null){
        if(req.body.bidPrice <= maxPrice[0].MaxPrice){
          req.body.idProduct = req.params.id;
          req.body.idUser = maxPrice[0].idUser;
          req.body.MaxPrice = maxPrice[0].MaxPrice;
          req.body.CurrentPrice =  Math.floor(req.body.bidPrice) +  Math.floor(product[0].BidJump);
          var dayStart = datetime.create();
          var formatDayStart = dayStart.format('m-d-Y H:M:S');
          req.body.BidTime = formatDayStart;
          productModel.bid(req.body).then(function () {
            sendDetailProductWithError(req.params.id, res)
          })
        }else{
          req.body.idProduct = req.params.id;
          req.body.idUser = req.session.idUser;
          req.body.MaxPrice = req.body.bidPrice;
          req.body.CurrentPrice = Math.floor(maxPrice[0].MaxPrice) + Math.floor(product[0].BidJump);
          var dayStart = datetime.create();
          var formatDayStart = dayStart.format('m-d-Y H:M:S');
          req.body.BidTime = formatDayStart;
          productModel.bid(req.body).then(function () {
            res.redirect('/detail/' + req.params.id)
          })
          ///Send Email: you're killed
        }
      }else{
        req.body.idProduct = req.params.id;
        req.body.idUser = req.session.idUser;
        req.body.MaxPrice = req.body.bidPrice;
        req.body.CurrentPrice = Math.floor(product[0].BidPrice) + Math.floor(product[0].BidJump);
        var dayStart = datetime.create();
        var formatDayStart = dayStart.format('m-d-Y H:M:S');
        req.body.BidTime = formatDayStart;
        productModel.bid(req.body).then(function () {
          res.redirect('/detail/' + req.params.id)
        })
      }
    }else if(fbPos.length != 0 && fbNeg.length == 0){
      if(maxPrice[0].idAuction !== null){
        if(req.body.bidPrice <= maxPrice[0].MaxPrice){
          req.body.idProduct = req.params.id;
          req.body.idUser = maxPrice[0].idUser;
          req.body.MaxPrice = maxPrice[0].MaxPrice;
          req.body.CurrentPrice =  Math.floor(req.body.bidPrice) +  Math.floor(product[0].BidJump);
          var dayStart = datetime.create();
          var formatDayStart = dayStart.format('m-d-Y H:M:S');
          req.body.BidTime = formatDayStart;
          productModel.bid(req.body).then(function () {
            sendDetailProductWithError(req.params.id, res)
          })
        }else{
          req.body.idProduct = req.params.id;
          req.body.idUser = req.session.idUser;
          req.body.MaxPrice = req.body.bidPrice;
          req.body.CurrentPrice = Math.floor(maxPrice[0].MaxPrice) + Math.floor(product[0].BidJump);
          var dayStart = datetime.create();
          var formatDayStart = dayStart.format('m-d-Y H:M:S');
          req.body.BidTime = formatDayStart;
          productModel.bid(req.body).then(function () {
            res.redirect('/detail/' + req.params.id)
          })
          ///Send Email: you're killed
        }
      }else{
        req.body.idProduct = req.params.id;
        req.body.idUser = req.session.idUser;
        req.body.MaxPrice = req.body.bidPrice;
        req.body.CurrentPrice = Math.floor(product[0].BidPrice) + Math.floor(product[0].BidJump);
        var dayStart = datetime.create();
        var formatDayStart = dayStart.format('m-d-Y H:M:S');
        req.body.BidTime = formatDayStart;
        productModel.bid(req.body).then(function () {
          res.redirect('/detail/' + req.params.id)
        })
      }
    }else{
      if(fbPos.length != 0){
        if((fbPos[0].sum + fbNeg[0].sum)/fbPos[0].sum >= 0.8){
          if(maxPrice[0].idAuction !== null){
            if(req.body.bidPrice <= maxPrice[0].MaxPrice){
              req.body.idProduct = req.params.id;
              req.body.idUser = maxPrice[0].idUser;
              req.body.MaxPrice = maxPrice[0].MaxPrice;
              req.body.CurrentPrice =  Math.floor(req.body.bidPrice) +  Math.floor(product[0].BidJump);
              var dayStart = datetime.create();
              var formatDayStart = dayStart.format('m-d-Y H:M:S');
              req.body.BidTime = formatDayStart;
              productModel.bid(req.body).then(function () {
                sendDetailProductWithError(req.params.id, res)
              })
            }else{
              req.body.idProduct = req.params.id;
              req.body.idUser = req.session.idUser;
              req.body.MaxPrice = req.body.bidPrice;
              req.body.CurrentPrice = Math.floor(maxPrice[0].MaxPrice) + Math.floor(product[0].BidJump);
              var dayStart = datetime.create();
              var formatDayStart = dayStart.format('m-d-Y H:M:S');
              req.body.BidTime = formatDayStart;
              productModel.bid(req.body).then(function () {
                res.redirect('/detail/' + req.params.id)
              })
              ///Send Email: you're killed
            }
          }else{
            req.body.idProduct = req.params.id;
            req.body.idUser = req.session.idUser;
            req.body.MaxPrice = req.body.bidPrice;
            req.body.CurrentPrice = Math.floor(product[0].BidPrice) + Math.floor(product[0].BidJump);
            var dayStart = datetime.create();
            var formatDayStart = dayStart.format('m-d-Y H:M:S');
            req.body.BidTime = formatDayStart;
            productModel.bid(req.body).then(function () {
              res.redirect('/detail/' + req.params.id)
            })
          }
        }
        else{
          NotAllowBid(req.params.id, res);
        }
      }
      else{
        NotAllowBid(req.params.id, res);
      }
    }
  })
})

function sendDetailProductWithError(id, res) {
  var entity = {
    id: id
  }
  q.all([productModel.getProductByID(entity), productModel.getTotalWatching(entity), productModel.bidLog(entity)])
  .spread(function (rows, total, bidLog) {
    if(!rows[0].Photo1){
      rows[0].Photo1 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
      rows[0].Photo2 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
    }
    else if (!rows[0].Photo2) {
      rows[0].Photo2 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
    }
    var date = new Date(rows[0].TimeEnd);
    rows[0].TimeEnd = date;
    var vm = {
      total: total[0].Total,
      product: rows[0],
      bidLog: bidLog,
      errorBid: true,
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
    };
    res.render('product/detailProduct', vm);
  })
}

function NotAllowBid(id, res) {
  var entity = {
    id: id
  }
  q.all([productModel.getProductByID(entity), productModel.getTotalWatching(entity), productModel.bidLog(entity)])
  .spread(function (rows, total, bidLog) {
    if(!rows[0].Photo1){
      rows[0].Photo1 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
      rows[0].Photo2 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
    }
    else if (!rows[0].Photo2) {
      rows[0].Photo2 = "http://www.freeiconspng.com/uploads/no-image-icon-11.PNG";
    }
    var date = new Date(rows[0].TimeEnd);
    rows[0].TimeEnd = date;
    var vm = {
      total: total[0].Total,
      product: rows[0],
      bidLog: bidLog,
      errorPer: true,
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
    };
    res.render('product/detailProduct', vm);
  })
}

route.get('/watchitem/:idProduct', function (req, res) {
  req.params.idUser = req.session.idUser;
  userModel.checkWatchItem(req.params).then(function (rows) {
    if(rows.length == 0){
      userModel.addWatchItem(req.params).then(function (data) {
        res.redirect('/detail/'+ req.params.idProduct)
      })
    }
    else{
      res.redirect('/detail/'+ req.params.idProduct)
    }
  })
})







module.exports = route;
