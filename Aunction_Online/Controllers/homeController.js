var express = require('express');
var productModel = require('../Models/productModel');
var datetime = require('node-datetime');
var helper = require('../Helper/helperFunctions');
var q = require('q');

var route = express.Router();

route.get('/', function(req, res) {
  if (req.session.idUser)
  {
    res.redirect('/home');
  }
  else{
    q.all([productModel.TopProductHighPrice(), productModel.TopProductEnd(), productModel.TopProductBid()])
    .spread(function (topHigh, topEnd, topBid) {
      var active = topHigh[0];
      topHigh.splice(0,1);
      topEnd.reverse();
      var vm = {
        hasUser: res.locals.layoutVM.user === undefined,
        layoutVM: res.locals.layoutVM,
        active: active,
        topHigh: topHigh,
        topEnd: topEnd,
        topBid: topBid
      };
      res.render('home/index', vm);
    })
  }
});

route.get('/home', function (req, res) {
  productModel.loadAllWithUser().then(function (rows) {
    for (var i = 0; i < rows.length; i++) {
       if(helper.getRemainTime(rows[i].TimeEnd).seconds < 0)
       {
         rows.splice(i,1);
         i = i - 1;
       }
       else{
        rows[i].TimeEnd = helper.getRemainTime(rows[i].TimeEnd).days + 'd ' + helper.getRemainTime(rows[i].TimeEnd).hours + 'h ' + helper.getRemainTime(rows[i].TimeEnd).minutes + 'm';
      }
    }
    var vm = {
      product: rows,
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
    };
    res.render('home/home' ,vm);
  });
});

module.exports = route;
