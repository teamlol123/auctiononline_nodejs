var express = require('express');
var	adminModel = require('../Models/adminModel');
var SHA256 = require("crypto-js/sha256");

var route = express.Router();

route.get('/', function(req,res) {
	var vm = {
		layout: false
	}
	res.render('admin/adminLogin', vm);
});

route.get('/userPermission', function(req,res) {
	adminModel.getUserbyUserAsking().then(function(rows) {
		var vm = {
			layout: false,
			user: rows
		}
		res.render('admin/userPermission', vm);
	})
});

route.get('/accept', function(req, res) {

	var id = req.query.id;

	adminModel.AcceptPermission(id).then(function(rows) {
		res.redirect('userPermission')	
	})
})

route.get('/deny', function(req, res) {

	var id = req.query.id;

	adminModel.resetUserAsking(id).then(function(rows) {
		res.redirect('userPermission');
	})
})


route.get('/userManagement', function(req,res) {
	adminModel.loadUser().then(function(rows) {
		var vm = {
		layout: false,
		user: rows
	}
	res.render('admin/userManagement', vm);
	})
});

route.get('/refresh', function(req,res) {
	var id = req.query.id;

	adminModel.refreshPassword(id).then(function(rows) {
		res.redirect('userManagement');
	})
})

route.get('/deleteUser', function(req,res) {
	var id = req.query.id;

	adminModel.deleteUser(id).then(function(rows) {
		res.redirect('userManagement');
	})
})

route.get('/categoryManagement', function(req,res) {
	adminModel.loadCategory().then(function(rows) {
		var vm = {
		layout: false,
		category: rows
	}
	res.render('admin/categoryManagement', vm);
	})
});

route.get('/addCategory', function(req,res) {
	var vm = {
		layout: false,
	}
	res.render('category/addCategory', vm);
})

route.post('/addCategory', function(req,res) {
	adminModel.insertCategory(req.body).then(function(data) {
		var vm = {
			layout: false
		}
		res.redirect('categoryManagement');
	})
})

route.get('/editCategory', function(req,res) {
	var id = req.query.id;
	adminModel.loadDetail(id).then(function(cat) {
		var vm = {
			layout: false,
			category: cat
		}
		res.render('category/editCategory', vm);
	})
})

route.post('/editCategory', function(req,res) {
	adminModel.updateCategory(req.body).then(function(rows) {
		res.redirect('categoryManagement');
	})
})

route.get('/delete', function(req,res) {
	var id = req.query.id;
	adminModel.deleteCategory(id).then(function(rows) {
		res.redirect('categoryManagement');
	})
})

route.post('/', function(req,res) {
	adminModel.getAdminbyUsername(req.body).then(function(rows) {
		if(rows.length == 0) {
			var vm = {
				    layout: false,
				    hasError : true,
				    error: "You input wrong Username or Password"
				  };
				 res.render('admin/adminLogin',vm);
		}else{
			req.body.inputPassword = SHA256(req.body.inputPassword);
			if(rows[0].PassWord == req.body.inputPassword) {
				res.redirect('adminLogin/userPermission');
			}else{
				var vm = {
				    layout: false,
				    hasError : true,
				    error: "You input wrong Username or Password"
				  };
				 res.render('admin/adminLogin',vm);
			}
		}
	});
});

module.exports = route;

