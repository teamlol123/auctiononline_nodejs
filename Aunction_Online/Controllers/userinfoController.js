var express = require('express');
var	userModel = require('../Models/userModel');
var productModel = require('../Models/productModel');
var helper = require('../Helper/helperFunctions');
var datetime = require('node-datetime');
var q = require('q');

var SHA256 = require("crypto-js/sha256");


var route = express.Router();


route.get('/', function(req, res) {
    var username = req.query.username;
    userModel.loadUserInfo(username).then(function(cat) {
        var vm = {
            hasUser: res.locals.layoutVM.user === undefined,
    		    layoutVM: res.locals.layoutVM,
            home: cat
        };
        res.render('user/userInfo', vm);
    });
});

route.post('/', function(req, res) {
    userModel.update(req.body).then(function(changedRows) {
        res.redirect('/');
    })
});

route.get('/changePassword', function(req,res) {
	res.render('user/changePassword');
});

route.post('/changePassword', function(req,res) {
	var username = req.query.username;
	userModel.loadUser(username).then(function(rows) {
		req.body.inputOldPass = SHA256(req.body.inputOldPass);
		req.body.inputNewPass = SHA256(req.body.inputNewPass);
		console.log(rows[0].Password);
		if (req.body.inputOldPass == rows[0].PassWord) {
			userModel.updatePass(req.body).then(function(changedRows) {
				var vm = {
					hasUser: res.locals.layoutVM.user === undefined,
    				layoutVM: res.locals.layoutVM,
    				isSuccess: true,
    				success: "You have SUCCESSFULLY change your password"
				};
				res.render('user/changePassword', vm);
			});
		}else{
			var vm = {
					  hasUser: res.locals.layoutVM.user === undefined,
    				layoutVM: res.locals.layoutVM,
    				hasError: true,
    				error: "You have enter your Old Password wrong"
				};
				res.render('user/changePassword', vm);
		}
	});
});

route.get('/watchlist', function (req, res) {
  productModel.getProductInWatch(req.session).then(function (rows) {
    var listClose = [];
    for (var i = 0; i < rows.length; i++) {
       if(helper.getRemainTime(rows[i].TimeEnd).seconds < 0)
       {
         listClose.push(rows[i]);
         rows.splice(i,1);
         i = i - 1;
       }
       else{
        rows[i].TimeEnd = helper.getRemainTime(rows[i].TimeEnd).days + 'd ' + helper.getRemainTime(rows[i].TimeEnd).hours + 'h ' + helper.getRemainTime(rows[i].TimeEnd).minutes + 'm';
      }
    }
    var vm = {
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
      products: rows,
      productsClose: listClose,
    }
    res.render('user/watchlist', vm);
  })
})

route.get('/yourlist', function (req, res) {
  q.all([productModel.getProductByUser(req.session), productModel.getProductWithWinner(req.session), productModel.getProductWithoutWinner(req.session)])
  .spread(function (product, pdWinner, pdNoWinner) {
    console.log(pdNoWinner);
    for (var i = 0; i < product.length; i++) {
       if(helper.getRemainTime(product[i].TimeEnd).seconds < 0)
       {
         product.splice(i,1);
         i = i - 1;
       }
       else{
        product[i].TimeEnd = helper.getRemainTime(product[i].TimeEnd).days + 'd ' + helper.getRemainTime(product[i].TimeEnd).hours + 'h ' + helper.getRemainTime(product[i].TimeEnd).minutes + 'm';
      }
    }
    // for (var i = 0; i < pdWinner.length; i++) {
    //   if(helper.getRemainTime(pdWinner[i].TimeEnd).seconds > 0) {
    //     pdWinner.splice(i,1);
    //     i = i - 1;
    //   }
    // }
    for (var i = 0; i < pdNoWinner.length; i++) {
      if(helper.getRemainTime(pdNoWinner[i].TimeEnd).seconds > 0) {
        pdNoWinner.splice(i,1);
        i = i - 1;
      }
    }
    var vm = {
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
      products: product,
      winner: pdWinner,
      nowinner: pdNoWinner
    }
    res.render('user/yourlist', vm);
  })
})

route.get('/detail', function (req, res) {

})

route.get('/yourbidon', function(req, res) {
  q.all([userModel.loadBidOn(req.session), productModel.getProductThatWin(req.session)])
  .spread(function (rows, pdWin) {
    var listClose = [];
    for (var i = 0; i < rows.length; i++) {
       if(helper.getRemainTime(rows[i].TimeEnd).seconds < 0)
       {
         listClose.push(rows[i]);
         rows.splice(i,1);
         i = i - 1;
       }
       else{
        rows[i].TimeEnd = helper.getRemainTime(rows[i].TimeEnd).days + 'd ' + helper.getRemainTime(rows[i].TimeEnd).hours + 'h ' + helper.getRemainTime(rows[i].TimeEnd).minutes + 'm';
      }
    }
    var vm = {
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
      products: rows,
      productsCLosed: listClose,
      productWin: pdWin,
    }
    res.render('user/yourbidon', vm);
  })

})

route.get('/asking', function(req,res) {
  var username = req.query.username;

  userModel.setUserAsking(username).then(function(rows) {
    res.redirect('/');
  })
})

route.get('/edit', function (req, res) {
  productModel.getProductByID(req.query).then(function (rows) {
    var vm = {
      product: rows[0],
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM
    }
    res.render("product/editProduct", vm)
  })
})

route.post('/edit', function (req, res) {
  productModel.getProductByID(req.query).then(function (rows) {
    var dayStart = datetime.create();
    var formatDayStart = dayStart.format('m-d-Y H:M:S');
    req.body.description = rows[0].Des + '<div style="text-align: center;">' + formatDayStart + '</div>' + req.body.description;
    req.body.id = req.query.id;
    console.log(req.body);
    productModel.updateDescription(req.body).then(function () {
      res.redirect("/detail/" + req.body.id);
    })
  })
})

route.get('/feedback', function (req, res) {
  userModel.getUserByIDUser(req.query).then(function (user) {
    var vm = {
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
      user: user[0]
    }
    res.render('user/feedback', vm)
  })
})

route.post('/feedback', function (req, res) {
  req.body.ToUser = req.query.idUser;
  req.body.FromUser = req.session.idUser;
  userModel.insertFeedback(req.body).then(function () {
    res.redirect('/');
  })
})

route.get('/listfeedback', function (req, res) {
  q.all([userModel.getFeedbackPosByUser(req.query), userModel.getFeedbackNegByUser(req.query), userModel.getSumPosFeedback(req.query), userModel.getSumNegFeedback(req.query)])
  .spread(function (rows, rowsNeg, sumPos, sumNeg) {
    if(sumPos[0].sum == null){
      sumPos = []
    }
    if(sumNeg[0].sum == null){
      sumNeg = []
    }
    var vm = {
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
      feedbackPos: rows,
      feedbackNeg: rowsNeg,
      sumPos: sumPos[0],
      sumNeg: sumNeg[0]
    }
    res.render('user/listfeedback', vm);
  })
})

route.get('/feedbackWin', function (req, res) {
  userModel.getUserByMaxPro(req.query).then(function (rows) {
    var vm = {
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
      user: rows[0]
    }
    res.render('user/feedbackWin', vm)
  })
})

route.post('/feedbackWin', function (req, res) {
  req.body.ToUser = req.query.idUser;
  req.body.FromUser = req.session.idUser;
  userModel.insertFeedback(req.body).then(function () {
    res.redirect('/');
  })
})

route.get('/kick', function (req, res) {
  productModel.bidLog(req.query).then(function (rows) {
    var vm = {
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
      user: rows
    }
    res.render('user/kick', vm)
  })
})


route.get('/kickUser', function (req, res) {
  userModel.kickUser(req.query).then(function () {
    res.redirect('/');
  })
})
module.exports = route;
