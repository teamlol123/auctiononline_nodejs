var express = require('express');

var route = express.Router();
route.get('/', function (req, res) {
  req.session.destroy();
  res.redirect('/signIn');
})
module.exports = route;
