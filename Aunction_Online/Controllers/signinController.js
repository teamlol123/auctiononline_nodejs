var express = require('express');
var	userModel = require('../Models/userModel');
var SHA256 = require("crypto-js/sha256");
var flash = require('connect-flash');

var route = express.Router();

route.get('/', function(req, res) {
  var vm = {
    hasUser: res.locals.layoutVM.user === undefined,
    layoutVM: res.locals.layoutVM,
    hasError : false,
  };
    res.render('home/signIn',vm);
});

route.post('/', function(req, res) {

	userModel.getUserbyUsername(req.body).then(function(rows) {
		if(rows.length == 0) {
			 var vm = {
				    hasUser: res.locals.layoutVM.user === undefined,
				    layoutVM: res.locals.layoutVM,
				    hasError : true,
				    error: "You input wrong Username or Password"
				  };
				 res.render('home/signIn',vm);
		}else{
			req.body.inputPassword = SHA256(req.body.inputPassword);
			if (rows[0].PassWord == req.body.inputPassword)
			{
				req.session.idUser = rows[0].idUser;
		        var hour = 1000 * 60 * 60 * 24;
		        req.session.cookie.expires = new Date(Date.now() + hour);
		        req.session.cookie.maxAge = hour;
				res.redirect('/home');
			}else{
				var vm = {
				    hasUser: res.locals.layoutVM.user === undefined,
				    layoutVM: res.locals.layoutVM,
				    hasError : true,
				    error: "You input wrong Username or Password"
				  };
				 res.render('home/signIn',vm);
			}
		}
	});
});

module.exports = route;
