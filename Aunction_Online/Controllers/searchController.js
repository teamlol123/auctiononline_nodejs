var express = require('express');
var categoryModel = require('../Models/categoryModel');
var productModel = require('../Models/productModel');
var helper = require('../Helper/helperFunctions');

var route = express.Router();
var q = require('q');

route.get('/', function (req, res) {
  q.all([categoryModel.loadAll(), productModel.loadAll()])
  .spread(function (cRows, pRows) {
    for (var i = 0; i < pRows.length; i++) {
      if(helper.getRemainTime(pRows[i].TimeEnd).seconds < 0){
        pRows.splice(i, 1);
        i = i - 1;
      }
    }
    var vm = {
      category: cRows,
      product: pRows,
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
    }
    res.render('category/search',vm);

  })
})

route.get('/:idCat', function (req, res) {
  q.all([categoryModel.loadAll(), productModel.getProductByIDCat(req.params)])
  .spread(function (cRows, pRows) {
    for (var i = 0; i < pRows.length; i++) {
      if(helper.getRemainTime(pRows[i].TimeEnd).seconds < 0){
        pRows.splice(i, 1);
        i = i - 1;
      }
    }
    var vm = {
      category: cRows,
      product: pRows,
      hasUser: res.locals.layoutVM.user === undefined,
      layoutVM: res.locals.layoutVM,
    }
    res.render('category/search',vm);
  })
})


route.post('/', function (req, res) {
  console.log(req.body.searchString);
  if(req.body.searchString === undefined)
  {
    res.redirect('/search');
  }
  else{
    if(req.body.Category != -1){
      q.all([categoryModel.loadAll(), productModel.searchProductWithCate(req.body)])
      .spread(function (cRows, pRows) {
        console.log(pRows);
        for (var i = 0; i < pRows.length; i++) {
          if(helper.getRemainTime(pRows[i].TimeEnd).seconds < 0){
            pRows.splice(i, 1);
            i = i - 1;
          }
        }
        var vm = {
          category: cRows,
          product: pRows,
          hasUser: res.locals.layoutVM.user === undefined,
          layoutVM: res.locals.layoutVM,
        }
        res.render('category/search',vm);
      })
  }
  else{
    q.all([categoryModel.loadAll(), productModel.searchProduct(req.body)])
    .spread(function (cRows, pRows) {
      console.log(pRows);
      for (var i = 0; i < pRows.length; i++) {
        if(helper.getRemainTime(pRows[i].TimeEnd).seconds < 0){
          pRows.splice(i, 1);
          i = i - 1;
        }
      }
      var vm = {
        category: cRows,
        product: pRows,
        hasUser: res.locals.layoutVM.user === undefined,
        layoutVM: res.locals.layoutVM,
      }
      res.render('category/search',vm);
    })
  }
  }
})

module.exports = route;
