var mustache = require('mustache'),
q = require('q'),
  db = require("../Helper/db");

  exports.getUserByIDUser = function (entity) {
    var sql = mustache.render(
      'select * from user where idUser = {{idUser}}',entity
    );
    return db.load(sql);
  }

exports.getMaxIDuser = function() {
	var sql = 'select * FROM user ORDER BY idUser DESC LIMIT 1';
	return db.load(sql);
}


exports.insertUser = function(entity) {
	var sql = mustache.render (
		'insert into user(idUser, Username, PassWord, Name, Email, Address) values ("{{idUser}}","{{inputUsername}}", "{{inputPassword}}" , "{{inputName}}", "{{inputEmail}}", "{{inputAddress}}")',
		entity
		);
	console.log(sql);
	return db.insert(sql);
}

 exports.getUserbyUsername = function (entity) {
 	var sql = mustache.render (
 		'select * FROM user where UserName = "{{inputUsername}}"',
 		entity
 		);
 	console.log(sql);
 	return db.load(sql);
 }

 exports.getUserbyEmail = function (entity) {
   var sql = mustache.render(
    'select * FROM user where Email = "{{inputEmail}}"',
    entity
    )
   return db.load(sql)
 }

exports.loadUserInfo = function(username) {
	var d = q.defer();

    var obj = {
        UserName: username
    };
    var sql = mustache.render(
        'select * from user where UserName = "{{UserName}}"',
        obj
    );
    console.log(sql);

    db.load(sql).then(function(rows) {
        d.resolve(rows[0]);
    });

    return d.promise;
}

exports.loadUser = function(username) {
  var obj = {
    UserName: username
  };

  var sql = mustache.render(
    'select * from user where UserName = "{{UserName}}"',
        obj
  );
  console.log(sql);
  return db.load(sql);
}

exports.update = function(entity) {
    var sql = mustache.render(
        'update user set Name = "{{inputName}}", Email = "{{inputEmail}}", Address = "{{inputAddress}}" where UserName = "{{inputUsername}}"',
        entity
    );
    console.log(sql);
    return db.update(sql);
}

exports.updatePass = function(entity) {
  var sql = mustache.render(
    'update user set PassWord = "{{inputNewPass}}"',
    entity
    );
  console.log(sql);
  return db.update(sql);
}

exports.addWatchItem = function (entity) {
  var sql = mustache.render(
    'insert into watchitem(idUser, idProduct) values ("{{idUser}}", "{{idProduct}}")', entity
  )
  console.log(sql);
  return db.insert(sql);
}

exports.checkWatchItem = function (entity) {
  var sql = mustache.render(
    'select * from watchitem where idProduct = {{idProduct}} and idUser = {{idUser}}', entity
  )
  console.log(sql);
  return db.load(sql);
}

exports.loadBidOn = function (entity) {
  var sql = mustache.render(
    "select p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, u.UserName from product p join auction_detail ad join user u where ad.idProduct = p.idProduct and p.idUser = u.idUser and ad.idUser = {{idUser}} group by ad.idProduct",
    entity
  )
  return db.load(sql);
}
exports.setUserAsking = function (username) {
  var obj = {
    UserName: username
  };

  var sql = mustache.render(
    'update user set UserAsking = "1" where UserName = "{{UserName}}"',
    obj
    )
  console.log(sql);
  return db.update(sql);
}

exports.insertFeedback = function (entity) {
  var sql = mustache.render(
    'insert into feedback (FromUser, ToUser, Rating, Comment) values ("{{FromUser}}", "{{ToUser}}", "{{rating}}", "{{comment}}")',
    entity
  )
  console.log(sql);
  return db.insert(sql);
}

exports.getFeedbackPosByUser = function (entity) {
  var sql = mustache.render(
    'SELECT * FROM feedback fb join user u where fb.FromUser = u.idUser and fb.rating = 1 and fb.ToUser = {{idUser}}',
    entity
  )
  console.log(sql);

  return db.load(sql);
}

exports.getFeedbackNegByUser = function (entity) {
  var sql = mustache.render(
    'SELECT * FROM feedback fb join user u where fb.FromUser = u.idUser and fb.rating = -1 and fb.ToUser = {{idUser}}',
    entity
  )
  console.log(sql);

  return db.load(sql);
}

exports.getSumPosFeedback = function (entity) {
  var sql = mustache.render(
    "SELECT sum(rating) as sum FROM feedback fb join user u where fb.FromUser = u.idUser and fb.rating = 1 and fb.ToUser = {{idUser}}",
    entity
  )
  return db.load(sql);
}

exports.getSumNegFeedback = function (entity) {
  var sql = mustache.render(
    "SELECT sum(rating) as sum FROM feedback fb join user u where fb.FromUser = u.idUser and fb.rating = -1 and fb.ToUser = {{idUser}}",
    entity
  )
  return db.load(sql);
}

exports.getUserByMaxPro = function (entity) {
  var sql = mustache.render(
    "select u.idUser , u.UserName, max(MaxPrice) from auction_detail ad join user u join product p where ad.idProduct = p.idProduct and ad.idUser = u.idUser and ad.idProduct = {{idProduct}}",
    entity
  )
  return db.load(sql);
}

exports.kickUser = function (entity) {
  var sql = mustache.render(
    "delete from auction_detail where idProduct = {{idProduct}} and idUser = {{idUser}}",
    entity
  )
  return db.delete(sql);
}
