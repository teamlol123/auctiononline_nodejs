var mustache = require('mustache'),
q = require('q'),
  db = require("../Helper/db");

exports.getAdminbyUsername = function (entity) {
 	var sql = mustache.render (
 		'select * FROM admin where UserName = "{{inputUsername}}"',
 		entity
 		);

 	return db.load(sql);
 }

 exports.getUserbyUserAsking = function () {
 	var sql = 'select * from user where UserAsking = "1"'

 	return db.load(sql);
 }

 exports.loadUser = function () {
 	var sql = 'select * from user'
 	console.log(sql);
 	return db.load(sql);
 }

exports.loadCategory = function () {
	var sql = 'select * from category'

	return db.load(sql);
  }

exports.insertCategory = function (entity) {
	var sql = mustache.render (
		'insert into category (Name) values ("{{inputName}}")',
		entity
		)

	return db.insert(sql);
}

exports.loadDetail = function(id) {
	var d = q.defer();

    var obj = {
        idCategory: id
    };

    var sql = mustache.render(
        'select * from category where idCategory = {{idCategory}}',
        obj
    );

    db.load(sql).then(function(rows) {
        d.resolve(rows[0]);
    });

    return d.promise;
}

exports.updateCategory = function (entity) {
	var sql = mustache.render (
		'update category set Name = "{{inputName}}" where idCategory = "{{inputId}}"',
		entity
		)

	return db.update(sql);
}

exports.deleteCategory = function(id) {

	var obj = {
		idCategory: id
	}
    var sql = mustache.render(
        'delete from category where idCategory = "{{idCategory}}"',
        obj
    );

    return db.delete(sql);
}

exports.deleteUser = function(id) {

	var obj = {
		idUser: id
	}
    var sql = mustache.render(
        'delete from user where idUser = "{{idUser}}"',
        obj
    );

    return db.delete(sql);
}

exports.refreshPassword = function (id) {

	var obj = {
		idUser: id
	}

	var sql = mustache.render(
		'update user set PassWord = "8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92" where idUser = {{idUser}}',
		obj
		);
	

	return db.update(sql);
}

exports.AcceptPermission = function (id) {

	var obj = {
		idUser: id
	}

	var sql = mustache.render(
		'update user set Permission = "1", UserAsking = "2" where idUser = "{{idUser}}"',
		obj 
		);

	return db.update(sql);
}

exports.resetUserAsking = function (id) {

	var obj = {
		idUser: id
	}

	var sql = mustache.render(
		'update user set UserAsking = "0" where idUser = "{{idUser}}"',
		obj 
		);

	return db.update(sql);
}