var mustache = require('mustache'),
  db = require("../Helper/db");

exports.insertProduct = function (entity) {
  var sql = mustache.render(
"insert into product(Name, MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, idUser) values ('{{title}}','{{{mainImage}}}','{{{photo1}}}', '{{{photo2}}}', '{{bidPrice}}','{{bidJump}}','{{instantPrice}}','{{{description}}}','{{{TimeStart}}}','{{{TimeEnd}}}','{{catID}}','{{idUser}}')",
     entity
  );
  console.log(sql);
  return db.insert(sql);
}

exports.getProductByIDCat = function (entity) {
  var sql = mustache.render(
    "select p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName from product p join user u where p.idUser = u.idUser and p.idCategory = {{idCat}}", entity
  );
  console.log(sql);
  return db.load(sql);
}

exports.loadAll = function () {
  var sql = 'select * from product';
  return  db.load(sql);
}

exports.loadAllWithUser = function () {
  var sql = 'SELECT p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName FROM product p join user u where p.idUser = u.idUser';
  return db.load(sql);
}

exports.getProductByID = function (entity) {
  var sql = mustache.render(
    "select * from product where idProduct = {{id}}", entity
  );
  return db.load(sql);
}

exports.searchProductWithCate = function (entity) {
  var sql = mustache.render(
    "select p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName from product p join user u where p.Name LIKE '%{{searchString}}%' and p.idUser = u.idUser and p.idCategory = {{Category}}", entity
  );
  return db.load(sql);
}

exports.searchProduct = function (entity) {
  var sql = mustache.render(
    "select p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName from product p join user u where p.idUser = u.idUser and p.Name LIKE '%{{searchString}}%'", entity
  )
  return db.load(sql);
}


exports.getProductInWatch = function (entity) {
  var sql = mustache.render(
    "SELECT p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName FROM watchitem w join product p join user u where w.idUser = u.idUser and p.idProduct = w.idProduct and w.idUser = {{idUser}}", entity
  )
  return db.load(sql);
}

exports.getProductByUser = function (entity) {
  var sql = mustache.render(
    "select p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName from product p join user u where p.idUser = u.idUser and u.idUser = {{idUser}}", entity
  )
  return db.load(sql);
}

exports.getTotalWatching = function (entity) {
  var sql = mustache.render(
    "SELECT count(*) as Total FROM watchitem where idProduct = {{id}}", entity
  )
  return db.load(sql);
}

exports.bid = function (entity) {
  var sql = mustache.render(
    "insert into auction_detail(idProduct, idUser, CurrentPrice, MaxPrice, BidTime) values ('{{idProduct}}', '{{idUser}}', '{{CurrentPrice}}', '{{MaxPrice}}', '{{{BidTime}}}')", entity
  )
  return db.insert(sql);
}
exports.bidLog = function (entity) {
  var sql = mustache.render(
    "SELECT ad.idProduct, u.idUser, u.UserName, ad.CurrentPrice, ad.BidTime FROM auction_detail ad join user u join product p where ad.idUser = u.idUser and ad.idProduct = p.idProduct and p.idProduct = {{id}} order by ad.BidTime asc",
    entity
  )
  return db.load(sql);
}

exports.getMaxBidPrice = function (entity) {
  var sql = mustache.render(
    "select idAuction,idProduct, idUser, max(MaxPrice) as MaxPrice,max(CurrentPrice) as CurrentPrice from auction_detail where idProduct = {{id}}",
    entity
  )
  return db.load(sql);
}

exports.updateDescription = function (entity) {
  var sql = mustache.render(
    "update product set Des = '{{{description}}}' where idProduct = {{id}}",
    entity
  )
  return db.update(sql);
}

exports.getProductWithWinner = function (entity) {
  var sql = mustache.render(
    "select p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName from product p join user u where exists (select * from auction_detail where p.idProduct = auction_detail.idProduct) and p.idUser = u.idUser and p.idUser = {{idUser}}",
    entity
  )
  return db.load(sql);
}

exports.getProductWithoutWinner = function (entity) {
  var sql = mustache.render(
    "select p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName from product p join user u where not exists (select * from auction_detail where p.idProduct = auction_detail.idProduct) and p.idUser = u.idUser and p.idUser = {{idUser}}",
    entity
  )
  console.log(sql);
  return db.load(sql);
}

exports.getProductThatWin = function (entity) {
  var sql = mustache.render(
    "select p.idProduct, p.Name , MainPhoto, Photo1, Photo2, BidPrice, BidJump, BuyPrice, Des, TimeStart, TimeEnd, idCategory, p.idUser, u.UserName from auction_detail AC join product p join user u where MaxPrice = (select Max(MaxPrice) from auction_detail ACtemp where ACtemp.idProduct = AC.idProduct) and p.idProduct = AC.idProduct and p.idUser = u.idUser and AC.idUser = {{idUser}}",
    entity
  )
  return db.load(sql);
}

exports.TopProductHighPrice = function () {
  var sql = "SELECT * FROM product order by BidPrice desc limit 5";
  return db.load(sql);
}

exports.TopProductEnd = function () {
  var sql = "SELECT * FROM product order by TimeEnd asc limit 5";
  return db.load(sql);
}

exports.TopProductBid = function () {
  var sql = "Select *, count(*) as SLXH from auction_detail ad join product p where ad.idProduct = p.idProduct group by ad.idProduct order by SLXH desc limit 5";
  return db.load(sql);
}

exports.getTurnBid = function (entity) {
  var sql = mustache.render(
    "select count(*) as turn from auction_detail where idProduct = {{id}}",
    entity
  )
  return db.load(sql);
}
