exports.getRemainTime = function (timeEnd) {
    var dayEnd = new Date(timeEnd).getTime();
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = dayEnd - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    var Remain = {
      days: days,
      hours: hours,
      minutes: minutes,
      seconds: seconds,
    }
    return Remain;
}
