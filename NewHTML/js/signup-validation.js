$(function() {
	$("#SignUp").validate({
		rules: {
			inputUsername: {
				required: true,
				minlength: 5
			},
			inputPassword: {
				required: true,
				minlength: 5
			},
			inputPasswordRe: {
				required: true,
				equalTo: "#inputPassword"
			},
			inputName: "required",
			inputEmail: {
				required: true,
				email: true
			},
			inputAddress: "required"
		},
		messages: {
			inputUsername: {
				required: "Please enter your username",
				minlength: "Your username must be atleast 5 character long"
			},
			inputPassword: {
				required: "Please enter your password",
			},
			inputPasswordRe :{
				required: "Please re-enter the password above"
			},
			inputName: "Please enter your name",
			inputEmail: {
				required: "Please enter your E-mail",
				email: "Please enter a valid E-mail address"
			},
			inputAddress: "Please enter your home address"
		}
	});
});