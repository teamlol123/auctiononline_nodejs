/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50717
Source Host           : 127.0.0.1:3306
Source Database       : auction

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-06-29 19:35:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `idAdmin` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) DEFAULT NULL,
  `PassWord` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idAdmin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'psphu', 'a497383adff6ba551f4241227f955f5cfe4c485dc49cf511ec33e089cb954d28');

-- ----------------------------
-- Table structure for auction_detail
-- ----------------------------
DROP TABLE IF EXISTS `auction_detail`;
CREATE TABLE `auction_detail` (
  `idAuction` int(11) NOT NULL AUTO_INCREMENT,
  `idProduct` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `CurrentPrice` int(11) NOT NULL,
  `MaxPrice` int(11) NOT NULL,
  `BidTime` varchar(45) NOT NULL,
  PRIMARY KEY (`idAuction`),
  KEY `fk_auction_product1_idx` (`idProduct`),
  KEY `fk_auction_user1_idx` (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auction_detail
-- ----------------------------
INSERT INTO `auction_detail` VALUES ('19', '12', '2', '5100', '5100', '06-29-2017 12:20:51');
INSERT INTO `auction_detail` VALUES ('20', '14', '4', '800100', '900000', '06-29-2017 12:23:35');
INSERT INTO `auction_detail` VALUES ('21', '10', '4', '910', '930', '06-29-2017 12:24:21');
INSERT INTO `auction_detail` VALUES ('22', '27', '5', '4010', '4500', '06-29-2017 12:25:06');
INSERT INTO `auction_detail` VALUES ('23', '27', '5', '4310', '4500', '06-29-2017 12:25:46');
INSERT INTO `auction_detail` VALUES ('24', '27', '2', '4510', '4600', '06-29-2017 12:26:03');
INSERT INTO `auction_detail` VALUES ('25', '19', '3', '155', '165', '06-29-2017 12:26:55');
INSERT INTO `auction_detail` VALUES ('26', '7', '3', '210', '250', '06-29-2017 17:08:07');
INSERT INTO `auction_detail` VALUES ('27', '9', '4', '90', '100', '06-29-2017 17:52:57');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `idCategory` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idCategory`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'Antiques');
INSERT INTO `category` VALUES ('2', 'Art');
INSERT INTO `category` VALUES ('3', 'Baby');
INSERT INTO `category` VALUES ('4', 'Books');
INSERT INTO `category` VALUES ('5', 'Cameras');
INSERT INTO `category` VALUES ('6', 'Cars, Vehicles & Parts');
INSERT INTO `category` VALUES ('7', 'Cell Phones');
INSERT INTO `category` VALUES ('8', 'Clothing, Shoes & Accessories');
INSERT INTO `category` VALUES ('9', 'Collectibles');
INSERT INTO `category` VALUES ('10', 'Computers & Networking');
INSERT INTO `category` VALUES ('11', 'Crafts');
INSERT INTO `category` VALUES ('12', 'Electronics');
INSERT INTO `category` VALUES ('13', 'Heath & Beauty');
INSERT INTO `category` VALUES ('14', 'Holiday & Seasonal');
INSERT INTO `category` VALUES ('15', 'Home & Garden');
INSERT INTO `category` VALUES ('16', 'Jewelry & Watches');
INSERT INTO `category` VALUES ('17', 'Movies & TV Shows');
INSERT INTO `category` VALUES ('18', 'Music & Instruments');
INSERT INTO `category` VALUES ('19', 'Pet');
INSERT INTO `category` VALUES ('20', 'Sporting Goods');
INSERT INTO `category` VALUES ('21', 'Video Games');
INSERT INTO `category` VALUES ('22', 'Other Stuff');

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `idfeedback` int(11) NOT NULL AUTO_INCREMENT,
  `FromUser` int(11) NOT NULL,
  `ToUser` int(11) NOT NULL,
  `Rating` int(11) NOT NULL,
  `Comment` text NOT NULL,
  PRIMARY KEY (`idfeedback`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of feedback
-- ----------------------------
INSERT INTO `feedback` VALUES ('2', '2', '3', '1', 'Yeahh your saxophone really good');
INSERT INTO `feedback` VALUES ('3', '4', '2', '-1', 'You are bad');
INSERT INTO `feedback` VALUES ('4', '4', '2', '-1', 'You are so bad');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `idProduct` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  `MainPhoto` varchar(45) NOT NULL,
  `Photo1` varchar(45) DEFAULT NULL,
  `Photo2` varchar(45) DEFAULT NULL,
  `BidPrice` int(11) NOT NULL,
  `BidJump` int(11) NOT NULL,
  `BuyPrice` int(11) DEFAULT NULL,
  `Des` varchar(500) NOT NULL,
  `TimeStart` varchar(45) NOT NULL,
  `TimeEnd` varchar(45) NOT NULL,
  `idCategory` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  PRIMARY KEY (`idProduct`),
  KEY `fk_product_category1_idx` (`idCategory`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('7', 'The name of the Wind', '/uploads/photos-1498710865159.jpg', '', '', '200', '10', '300', '<p>This books is fukcing awesomeeee!!</p>', '06-29-2017 11:34:25', '07-06-2017 11:34:25', '4', '2');
INSERT INTO `product` VALUES ('8', 'Iphone9', '/uploads/photos-1498711053404.jpg', '', '', '600', '10', '900', '<p style=\"text-align: left;\"><em>This is the most &nbsp;<strong>FUTURISTIC&nbsp;</strong>phone in the whole universe! Nothing can compare to it! So suck it and give us money</em></p>', '06-29-2017 11:37:33', '07-03-2017 11:37:33', '7', '2');
INSERT INTO `product` VALUES ('9', 'shirt', '/uploads/photos-1498711286027.jpg', '', '', '80', '10', '500', '<p>A Blue Freaking shirt</p>', '06-29-2017 11:41:26', '07-08-2017 11:41:26', '8', '2');
INSERT INTO `product` VALUES ('10', 'Laptop', '/uploads/photos-1498711376717.jpg', '/uploads/photos-1498711376718.jpg', '', '900', '10', '1000', '<p>Gaming suitable for all kind of gamer!</p><div style=\"text-align: center;\">06-29-2017 12:22:15</div><p>Gamer laptop! A must have for anyone into game and stuff</p>', '06-29-2017 11:42:56', '07-05-2017 11:42:56', '10', '2');
INSERT INTO `product` VALUES ('11', 'Goop', '/uploads/photos-1498711525510.jpg', '', '', '60', '10', '100', '<p>Healthy food&nbsp;</p>', '06-29-2017 11:45:25', '07-05-2017 11:45:25', '13', '3');
INSERT INTO `product` VALUES ('12', 'lion', '/uploads/photos-1498711606108.jpg', '', '', '5000', '100', '150000', '<p>Very dangerous animal</p>', '06-29-2017 11:46:46', '07-09-2017 11:46:46', '19', '3');
INSERT INTO `product` VALUES ('13', 'Messi&#39;s shose', '/uploads/photos-1498711691974.jpg', '', '', '8000', '100', '900000', '<p>Used to be wore by Lionel Messi! Extremely rare</p>', '06-29-2017 11:48:11', '07-07-2017 11:48:11', '20', '3');
INSERT INTO `product` VALUES ('14', 'Futuristic House', '/uploads/photos-1498711757335.jpg', '', '', '800000', '100', '1000000', '<p>Not for casual</p>', '06-29-2017 11:49:17', '07-06-2017 11:49:17', '15', '3');
INSERT INTO `product` VALUES ('15', 'House Chair', '/uploads/photos-1498711835745.jpg', '', '', '90', '10', '200', '<p>A homemade red chair!</p>', '06-29-2017 11:50:35', '07-04-2017 11:50:35', '11', '3');
INSERT INTO `product` VALUES ('16', 'Rare Stamp', '/uploads/photos-1498712085429.jpg', '/uploads/photos-1498712085431.jpg', '/uploads/photos-1498712085437.jpg', '100000', '100', '500000', '<p>Ultra rare stuff!</p>', '06-29-2017 11:54:45', '07-04-2017 11:54:45', '9', '2');
INSERT INTO `product` VALUES ('17', 'Clepatra&#39;s Neckless', '/uploads/photos-1498712265380.jpg', '/uploads/photos-1498712265383.jpg', '', '700000', '100', '890000', '<p>Ancient Necklace may or may not be cursed</p>', '06-29-2017 11:57:45', '07-06-2017 11:57:45', '16', '4');
INSERT INTO `product` VALUES ('18', 'Die Hard DVD', '/uploads/photos-1498712395605.jpg', '', '', '100', '10', '500', '<p>Awesome movie! A must watch</p>', '06-29-2017 11:59:55', '07-03-2017 11:59:55', '17', '4');
INSERT INTO `product` VALUES ('19', 'Kindle', '/uploads/photos-1498712469539.jpg', '', '', '145', '10', '300', '<p>For reading books! Can also surfweb too</p>', '06-29-2017 12:01:09', '07-05-2017 12:01:09', '12', '4');
INSERT INTO `product` VALUES ('20', 'Batman: Arkham City', '/uploads/photos-1498712557751.jpg', '', '', '60', '10', '100', '<p>Game of the year</p>', '06-29-2017 12:02:37', '07-05-2017 12:02:37', '21', '4');
INSERT INTO `product` VALUES ('21', 'Mistborn: The Final Empire', '/uploads/photos-1498712678700.jpg', '', '', '20', '1', '30', '<p>One of Brandon Sanderson best</p>', '06-29-2017 12:04:38', '07-06-2017 12:04:38', '4', '5');
INSERT INTO `product` VALUES ('22', 'The Way of Kings', '/uploads/photos-1498712754778.jpg', '/uploads/photos-1498712754787.jpg', '', '25', '1', '60', '<p>Even better than Mistborn! A must read</p>', '06-29-2017 12:05:54', '07-06-2017 12:05:54', '4', '5');
INSERT INTO `product` VALUES ('23', 'Warbreaker', '/uploads/photos-1498712820620.jpg', '', '', '30', '1', '50', '<p>Stunningly beautiful world and story</p>', '06-29-2017 12:07:00', '07-05-2017 12:07:00', '4', '5');
INSERT INTO `product` VALUES ('24', 'Ferrari', '/uploads/photos-1498712930694.jpg', '', '', '500000', '200', '1000000', '<p>The car of you dream</p>', '06-29-2017 12:08:50', '07-07-2017 12:08:50', '6', '5');
INSERT INTO `product` VALUES ('25', 'Rolex golden Watch', '/uploads/photos-1498713029092.jpg', '', '', '9999', '100', '20000', '<p>Looks so good you wanna jizz your pants</p>', '06-29-2017 12:10:29', '07-05-2017 12:10:29', '16', '5');
INSERT INTO `product` VALUES ('26', 'Lost child', '/uploads/photos-1498713113689.jpg', '', '', '1000000', '100', '5000000', '<p>You should not buy this...&nbsp;</p>', '06-29-2017 12:11:53', '07-05-2017 12:11:53', '3', '4');
INSERT INTO `product` VALUES ('27', 'Saxophone', '/uploads/photos-1498713191698.jpg', '', '', '4000', '10', '5000', '<p>Sound ok! Good for playing music</p>', '06-29-2017 12:13:11', '07-05-2017 12:13:11', '17', '3');
INSERT INTO `product` VALUES ('28', 'Beach gear', '/uploads/photos-1498713257983.jpg', '', '', '60', '1', '100', '<p>For family and friends</p>', '06-29-2017 12:14:17', '07-05-2017 12:14:17', '14', '2');
INSERT INTO `product` VALUES ('29', 'Cannon', '/uploads/photos-1498713354419.jpg', '', '', '1000', '20', '5000', '<p>Produce very good photo</p>', '06-29-2017 12:15:54', '07-06-2017 12:15:54', '5', '5');

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO `sessions` VALUES ('hKtQq6xPto7PlqTHXPaie8zGQ6_8OetA', '1498800436', 0x7B22636F6F6B6965223A7B226F726967696E616C4D6178416765223A38363430303030302C2265787069726573223A22323031372D30362D33305430353A32363A32362E3930325A222C22687474704F6E6C79223A747275652C2270617468223A222F227D2C22696455736572223A337D);
INSERT INTO `sessions` VALUES ('rcyzvRbn5UAIS2mQ3WSJfxehvWFJ6NLH', '1498826085', 0x7B22636F6F6B6965223A7B226F726967696E616C4D6178416765223A6E756C6C2C2265787069726573223A6E756C6C2C22687474704F6E6C79223A747275652C2270617468223A222F227D7D);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `UserName` varchar(45) NOT NULL,
  `PassWord` varchar(500) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Address` varchar(45) DEFAULT NULL,
  `Permission` int(11) DEFAULT '0',
  `UserAsking` int(11) DEFAULT '0',
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'psphu', 'a497383adff6ba551f4241227f955f5cfe4c485dc49cf511ec33e089cb954d28', 'phu', 'psphu96@gmail.com', '29 Bau Cat 7', '0', '0');
INSERT INTO `user` VALUES ('2', 'test1', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'test1', 'mailbot686@gmail.com', '1', '1', '2');
INSERT INTO `user` VALUES ('3', 'test2', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'test2', 'mailbot687@gmail.com', '2', '1', '2');
INSERT INTO `user` VALUES ('4', 'test3', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'test3', 'mailbot688@gmail.com', '3', '1', '2');
INSERT INTO `user` VALUES ('5', 'test4', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'test4', 'mailbot689@gmail.com', '4', '1', '2');

-- ----------------------------
-- Table structure for watchitem
-- ----------------------------
DROP TABLE IF EXISTS `watchitem`;
CREATE TABLE `watchitem` (
  `idProduct` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of watchitem
-- ----------------------------
INSERT INTO `watchitem` VALUES ('12', '2', '11');
INSERT INTO `watchitem` VALUES ('16', '4', '12');
INSERT INTO `watchitem` VALUES ('25', '4', '13');
INSERT INTO `watchitem` VALUES ('29', '4', '14');
INSERT INTO `watchitem` VALUES ('7', '5', '15');
INSERT INTO `watchitem` VALUES ('8', '5', '16');
INSERT INTO `watchitem` VALUES ('9', '5', '17');
INSERT INTO `watchitem` VALUES ('24', '5', '18');
INSERT INTO `watchitem` VALUES ('20', '3', '19');
INSERT INTO `watchitem` VALUES ('21', '3', '20');
